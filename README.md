# Vegoa Violations Tracker

The violations tracker can be used to report any violations you encounter in regards to the Vegoa manifest and Vegoa's values.
Reporting these issues may help update the manifest to a more complete version in order to avoid any further violation in the future.
This violations tracker is a public tool that anyone, including non-members of Vegoa itself, can contribute to. Therefore each violation report will be discussed and you are welcome to contribute on others violations report.
This tool will allow help find solutions to this issues and, if needed, take action against the people violating Vegoa's manifest and values.
Vegoa is an association and if more than 50% of its members agree, they can take action/expell another member.

* **The Vegoa Manifest is available [here](https://gitlab.com/vegoa/manifest/blob/master/Manifest.md).**
* **The Violations Tracker is available [here](https://gitlab.com/vegoa/violations/issues).**

## Code of Conduct

The community is one of the best features of Vegoa, and we want to ensure it remains welcoming and safe for everyone. 
We have adopted the Contributor Covenant for all projects in the @Vegoa Gitlab group, the discussion forum, chat rooms, mailing list, social media tools, meetups and any other public event related to Vegoa.
This code of conduct outlines the expectations for all community members, as well as steps to report unacceptable behavior. 
We are committed to providing a welcoming and inspiring community for all and expect our code of conduct to be honored.

* **The Code of Conduct is available [here](https://gitlab.com/vegoa/violations/blob/master/CODE_OF_CONDUCT.md).**